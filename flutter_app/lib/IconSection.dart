import 'package:flutter/material.dart';
import 'package:flutter_app/IconTag.dart';

class IconSection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row (
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget> [
        IconTag(Colors.lightBlue, Icons.music_note, 'House Music'),
        IconTag(Colors.lightBlue, Icons.flight_takeoff, 'Travel'),
        IconTag(Colors.lightBlue, Icons.smoking_rooms, 'Smoking')
      ],
    );
  }
}