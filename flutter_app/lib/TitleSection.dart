import 'package:flutter/material.dart';

class TitleSection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container (
      padding: const EdgeInsets.all(32),
      child: Row (
        children: <Widget> [
          Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget> [
                  Container (
                    padding: const EdgeInsets.only(bottom: 8),
                    child: Text('Malaa', style: TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Text('DJ & Music Producer', style: TextStyle(color: Colors.grey))
                ],
              )
          ),
          Icon(Icons.home, color: Colors.deepOrange),
          Text('5')
        ],
      ),
    );
  }
}