import 'package:flutter/material.dart';
import 'package:flutter_app/TitleSection.dart';
import 'package:flutter_app/IconSection.dart';
import 'package:flutter_app/DescriptionSection.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Welcome to Flutter',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Homework 2'),
        ),
        body: Column(
          children: <Widget> [
            Image.asset('./images/malaa.jpg'),
            TitleSection(),
            IconSection(),
            DescriptionSection()
          ]
        )
      ),
    );
  }
}