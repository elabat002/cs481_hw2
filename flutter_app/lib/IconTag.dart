import 'package:flutter/material.dart';

class IconTag extends StatelessWidget {
  IconTag(this.color, this.icon, this.iconLabel);
  final Color color;
  final IconData icon;
  final String iconLabel;

  @override
  Widget build(BuildContext context) {
    return Column (
      children: <Widget> [
        Icon(icon, color: color),
        Container (
          padding: const EdgeInsets.only(top: 8),
          child: Text(iconLabel, style: TextStyle(fontSize: 12, color: color)),
        )
      ],
    );
  }
}