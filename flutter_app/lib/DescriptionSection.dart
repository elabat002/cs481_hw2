import 'package:flutter/material.dart';

class DescriptionSection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container (
      padding: const EdgeInsets.all(32),
      child: Text('Malaa is a French electronic music DJ and producer, who is signed to Tchami\'s label Confession. '
          'He broke onto the electronic music scene through his single "Notorious", which was the second release on Confession. '
          'His identity is unknown as he appears in public as a balaclava-wearing man.',
        softWrap: true,
      ),
    );
  }
}